  注意：日历控件的右键菜单依赖于jquery.contextMenu.js组件
  
  组件属性说明
  elem:必需项 使用jq选择器
  
  holidayType:假日的类型 
   默认值为
   [{
       id:0,
       menuitem:
           [{
               key:'add',
               name:'添加假日',
               icon:'add'
           }]
   },
   {
       id:1,
       name:'节假日',
       caption:'假',
       daycolor:'#000000',
       color:'#f52e2e',
       menuitem:
           [{
               key:'modify',
               name:'修改假日',
               icon:'edit'
           },
           {
               key:'delete',
               name:'删除假日',
               icon:'delete'
           }]
    },
   {
       id:2,
       name:'补班',
       caption:'班',
       daycolor:'#FF0000',
       color:'#cbcbcb',
       menuitem:
           [{
               key:'modify',
               name:'修改假日',
               icon:'edit'
           },
           {
               key:'delete',
               name:'删除假日',
               icon:'delete'
           }]
   }]
   id表示假日类型 name表示假日类型的名称 caption表示在日历中显示的标识文字 daycolor表示日期文字的颜色 color表示caption标识文字的颜色 
   menuitem表示该日期类型显示的右键菜单 key 菜单的键值 name 菜单显示的名称 icon 菜单显示的图标
  
   
  holidayurl:查询月份假日信息的请求地址 GET模式 参数为month=yyyy-MM，返回的数据格式为{Success:true,Data:[],Msg:null},Success表示请求数据是否成功,Data为假日信息的列表.
  
  value:默认当前显示的月份 格式为yyyy-MM
  
  menucallback: function (itemkey, obj){} 菜单的回调方法 itemKey 表示菜单的key值,obj表示返回的日期对象
  obj数据的结构
  Date 触发菜单的日期 格式为yyyy-MM-dd
  DateType 日期类型 对应holidayType的id值
  HolidayID 假期的后台ID号
  BeginDate 假期的开始时间 格式 yyyy-MM-dd HH:mm:ss
  EndDate 假期的结束时间 格式 yyyy-MM-dd HH:mm:ss
  
  假日信息数据格式
  {
       ID:'1',//假日对应的ID值
       DateType:1,//假期类型 对应holidayType的id值
       BeginDate:'2021-01-01 00:00:00',//开始日期
       EndDate:'2021-01-03 23:59:59'//结束日期 范围时间使用
  }
  
  =============================================================
  调用示例
  html
  引用css
  <link href="~/lib/jquerycontextmenu/jquery.contextMenu.css" rel="stylesheet" />
   <link href="~/lib/layui/ext/layCalendar.css" rel="stylesheet" />
   引用js
   <script type="text/javascript" src="~/lib/jquerycontextmenu/jquery.contextMenu.js"></script>
   
  <div id="calendar1" lay-filter="calendar"></div>
  
  
  js
  layui.use(['jquery', 'layCalendar'], function () {
  var $ = layui.$;
  var layCalendar = layui.layCalendar;
  初始化控件
  layCalendar.render({
       elem: '#calendar1',
       holidayurl: '/HolidayInfo/GetHolidayByMonth',
       menucallback: function (itemKey, data) {
 
       }
   });
   刷新控件
   layCalendar.reload('calendar');
   });
![输入图片说明](https://images.gitee.com/uploads/images/2021/0617/161325_cb9be083_1218076.png "截图00.png")